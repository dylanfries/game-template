﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformWidget : MonoBehaviour {

	// default values
	public Vector3 movementVector = new Vector3();
	public Vector3 rotationVector = new Vector3();
	public Vector3 scaleVector = new Vector3(1f, 1f, 1f);

	// Cached Variables
	private Transform _cachedTransform;

	// Use this for initialization
	void Start () {
		_cachedTransform = GetComponent<Transform>();
		
	}
	
	// Update is called once per frame
	void Update () {

		// Movement
		_cachedTransform.Translate(movementVector);

		// Rotation
		_cachedTransform.Rotate( rotationVector );

		// Scale
		_cachedTransform.localScale = scaleVector;

	}
}
