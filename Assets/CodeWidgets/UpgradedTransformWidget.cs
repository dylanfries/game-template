﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradedTransformWidget : MonoBehaviour {

	// default values
	// add controls for movement over time
	[Header("Movement Controls")]
	public bool isMovementEnabled = true; // start on, but usually I'll start things like this off, choose a good default!
	public Vector3 movementVector = new Vector3();

	[Header("Additional Movement Controls")]
	public bool pingPongMovement = false;
	public float rangeFromOrigin = 5f;

	private Vector3 _cachedStartingLocation;

	public float cooldownTimerStartingValue = 0.02f; // the amount of time to wait before firing again
	private float currentCooldownCounter = 0;

	[Header("Rotation Controls")]
	public bool isRotationVector = true; //
	public Vector3 rotationVector = new Vector3();

	[Header("Scale Controls")]
	public bool isScaleVector = true;
	public Vector3 scaleVector = new Vector3(1f, 1f, 1f);

	// Cached Variables
	private Transform _cachedTransform;

	// Use this for initialization
	void Start () {
		_cachedTransform = GetComponent<Transform>();
		_cachedStartingLocation = _cachedTransform.position; // Cache the position

	}
	
	// Update is called once per frame
	void Update () {

		// Movement
		if( isMovementEnabled ){
			// Time.deltaTime
			// This is ( 1 / frames per second ) 
			_cachedTransform.Translate(movementVector * Time.deltaTime );

			if (pingPongMovement) {
				CheckDistanceForPingPong();
				// If the timer is finished, trigger the effect
				if (currentCooldownCounter > cooldownTimerStartingValue) {



				 
				}else{
				
					// Else the timer is not complete so we add to it . 

					// Time.deltaTime is the amount of time that it took to complete the last frame
					// Not the most accurate way of doing timers but its the simplest
					currentCooldownCounter = currentCooldownCounter + Time.deltaTime;

					// currentCooldownCounter += Time.deltaTime; // Same as above line shorthand
				}
			}
		}
	
		// Rotation
		if( isRotationVector ){
			_cachedTransform.Rotate(rotationVector * Time.deltaTime );
		}

		// Scale
		if( isScaleVector){
			_cachedTransform.localScale = scaleVector;
		}
	}

	private void CheckDistanceForPingPong(){
		if (Vector3.Distance(_cachedStartingLocation, _cachedTransform.position) > rangeFromOrigin) {
			PingPongMovement();
		}
	}

	// Reverse the direction of the transform. 
	public void PingPongMovement(){
		
		movementVector = movementVector * -1f;

		// Reset the Cooldown Timer. Put this in the function for the cooldown to reset when the player triggers it. 
		currentCooldownCounter = cooldownTimerStartingValue;

	}
}
