﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// UI
using UnityEngine.UI;

public class AdventureController : MonoBehaviour {

	// UI Parent panel
	[Header("UI References")]
	public GameObject playerCreationPanel;
	//// This is the UI element that displays the text
	public Text storyBeatText;

	[Header("Audio")]
	public AudioSource playerSelectionFX;

	// Source image
	//	public Sprite playerPortrait; // [ ] to do : not yet used

	[Header("Player Display")]
	// Sprite Display
	public SpriteRenderer sprite;

	[Header("Story Text")]
	// This is the string variable we want to communicate the text with
	public string openingText = "And So our Story Began... ";
	public string playerSelected = "Factional alliances are deep, you have been born and raised by the ";

	[Header("Player State")]
	public bool hasGameBegun = false;

	[Header("Input")]
	bool button1Input = false;
	float horizontalInput = 0f;

	// Special Unity specific function that is called at startup. Use this for initialization
	void Start () {

		//storyBeatText.text = openingText;
		//characterSelectionPanel.SetActive(true);
		BeginTheGame();
	}

	public void BeginTheGame(){

	}

	public void SetPlayerArtRed(){
		// sets the sprites color to be red
		sprite.color = Color.red;
		playerCreationPanel.SetActive(false);
		storyBeatText.text = playerSelected + "Red Smashers";
		hasGameBegun = true;
		playerSelectionFX.Play();
	}

	public void SetPlayerArtBlue(){
		sprite.color = new Color(0, 1f, .5f);
		playerCreationPanel.SetActive(false);
		storyBeatText.text = playerSelected + "Green Boogies";
		hasGameBegun = true;
		playerSelectionFX.Play();
	}
	// Update 

	// Special Unity Function that is automatically called once per frame
	void Update () {

		button1Input = Input.GetButton("Jump");
		horizontalInput = Input.GetAxis("Horizontal");

		if(hasGameBegun){

			// Get input from player
			//			Input.GetKey(KeyCode.A);
			// Get hte specific key.



			// Get 

		}
	}
}
