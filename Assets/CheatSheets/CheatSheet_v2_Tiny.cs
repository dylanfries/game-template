﻿// Dylans Unity Cheat Sheet V2 - March 9th

using UnityEngine; // Defaults
using System.Collections; // Defaults
using System.Collections.Generic; // This Is For Lists
using UnityEngine.UI; // Add this to use the UI elements

// Basic Class
public class CheatSheet_v2_Tiny : MonoBehaviour {

  // Variable types
	public bool isKeyDown = false;  // Logical Operator
	public int currentTurnNumber = 0; // Basic Integers
	public float floatPointValue = 1.314f; // don't forget the f!
	public int[] arrayOfInts; // an array can hold many of an item
	public List<int> listOfInts = new List<int>(); // Generic List
	public List<string> whatHappenedThisTurnLog = new List<string>();

	public bool printAdditionalMessage = true;
	public string message = "So Far So Good!";

	private GameObject _cachedGameObject;
	private Transform _cachedTransform;

	// Physics
	private Rigidbody2D _rigid;

	// Effects
	public AudioSource sfx;
	public SpriteRenderer art;
	public ParticleSystem particles;
	public Animator animator;
		
	// UI
	public Text textDisplay; 
	public Image uiImage;
	public Button clickableButton;

	// Item To Spawn
	public GameObject itemPrefab;

	void SampleFunction(){
		
		// Debugging
		Debug.Log(message);
		Debug.DrawLine(Vector3.zero, Vector3.up, Color.red);

		// Input Functions
		Input.GetKeyDown(KeyCode.A);
		Input.GetButtonDown("Jump");
		Input.GetAxis("Horizontal");

		// Basic Movement
		Vector3 moveVector = new Vector3(0, 1f, 0);
		transform.Translate(moveVector);
		transform.Rotate(moveVector);
	}

	// Built In Functions (MonoBehaviour)
	void Update(){} // Every Frame
	void Awake(){} // First at startup
	void Start(){ // After Awake at startup
		// Get a component
		_cachedGameObject = GetComponent<GameObject>();
		// Register button Listener
		clickableButton.onClick.AddListener(ButtonPressed);
	} 

	// Update UI
	public void UpdateUI(){
		textDisplay.text = "Hello World";
	}
	// UI Button Pressed
	private void ButtonPressed(){
		// Effects
		sfx.Play(); // Sounds
		particles.Emit(5); // emit 5 particles
		particles.Play(); // play the particle system loop
		art.color = Color.red; // make the sprite red
		uiImage.color = Color.blue; // make the UI image blue
		animator.SetTrigger("TriggerName"); // Set Trigger "TriggerName" in Animator

		// Spawning
		GameObject go = Instantiate(itemPrefab, transform.position, transform.rotation);
		go.name = "New Game Object"; // assign name
		Destroy(go, 3f); // Destroy new game object in 3 seconds

		// Offset in Y
		go.transform.Translate(new Vector3(0, Random.Range(-5f, 5f), 0));
	}

	// CollisionEvents()
	private void OnCollisionEnter2D(Collision2D collision){}
	private void OnTriggerEnter2D(Collider2D collision){}
}
