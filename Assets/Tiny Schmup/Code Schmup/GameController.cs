﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	[Header("Story Manager")]
	public StoryManager storyManager;

	[Header("Input")]
	public bool buttonDown = false;
	public float horizontalInput = 0;
	public float horizontalMoveSpeed = 1f;
	public float forwardSpeed = 1f;

	// limit the width the player can move
	public float maxHorizontalDistance = 26f;

	[Header("Images")]
	public SpriteRenderer buttonImage;
	public GameObject crashSitePrefab;

	[Header("Sound Effects")]
	public AudioSource shipSFX;

	[Header("Object References")]
	public Transform playerShipTransform;

	[Header("UI Object References")]
	public GameObject titleScreen;
	public GameObject failedScreen;
	public GameObject successScreen;

	[Header("Game State")]
	public bool gameHasBegun = false;
	public bool gameHasEnded = false;

	// Save this for control class or animation
	[Header("Lerp")]
	private float lerpValue = 0; // Holds hte value of the lerp
	public float lerpSpeed = 1;

	// Fade out Timer
	[Header("Fade Out Timer")]
	public float timeToFadeOut = 2f;

	// Use this for initialization
	void Start() {
		InitializeGame();
	}

	void InitializeGame() {
		// set up level
		gameHasBegun = false;
		gameHasEnded = false;
		// set up player

		// show title screen
		OpenTitleScreen();

		// begin the game 

	}

	// Opens the Title Screen
	void OpenTitleScreen() {
		titleScreen.SetActive(true);

	}


	// Closes the Title Screen
	public void CloseTitleScreen() {
		// Disable title button
		titleScreen.SetActive(false);

		gameHasBegun = true;
		shipSFX.Play();

		// Start the story panels
		storyManager.GameBegins();
	}

	// Open End Of Game Screen

	public void HitARock(){
		// Destroy the player ship
		playerShipTransform.gameObject.SetActive(false);
		// Spawn a crash site
		// Could either have it ready to go or spawn it new. 
		// Keeping these prespawned and just turning them on is an optimization strategy called pooling that we will not get into.
		// Unless you spawning tons and tons its fine for now, BUT instantiate is somewhat expensive of an operation to do all the time
		GameObject spawnedObject = Instantiate(crashSitePrefab, playerShipTransform.position, playerShipTransform.rotation);

		// I Intermediate tip : You can add the forth field of instantiate to add it under a target parent in the hierarchy, which is handy for keeping the tree organized at runtime
		// I eg  GameObject spawnedObject = Instantiate(crashSitePrefab, playerShipTransform.position, playerShipTransform.rotation, parentReference);

		// Wait and then reset

		// Call the story manager and let it know the game ended. 
		storyManager.GameCrashedEnding();
		//		crashSitePrefab
	}

	public void HitAnEnemy(GameObject enemy){
		playerShipTransform.gameObject.SetActive(false);
		GameObject spawnedObject = Instantiate(crashSitePrefab, playerShipTransform.position, playerShipTransform.rotation);
		storyManager.EnemyKilledPlayerEnding(enemy);
	}

	public void GameEndedSuccessfully(StoryBeat beat){

		// Stop the ship
		gameHasEnded = true; // end the game

		// Story Beat happens
		storyManager.SuccessfulEnd(beat);
	}


	// Next Steps: This could all be moved into the ShipController itself. 
	// Update is called once per frame
	void Update() {

		// Get Basic Test Button Down
		buttonDown = Input.GetButton("Jump");

		// has the title been closed and the setup completed?
		if (gameHasBegun && !gameHasEnded) {


			// 3. 
			// Get float representing left / right input (-1,1)
			horizontalInput = Input.GetAxis("Horizontal");
			// Initialize a new Vector
			Vector3 inputVector = Vector3.zero;
			// Make the Vector x value equal to horizontal input
			// Ship will move (horizontalInput) units per frame
			//inputVector.x = horizontalInput * moveSpeed;

			// 3b. 
			// Delta time is the time the last frame took, so the time since the last Update was called. 
			// It can be useful in timers but in this case we use it to convert units per frame into units per second
			// For example, if we are running at 60fps, Time.deltaTime is ~ 1/60th of a second, therefor horizontalInput/frame * frames/second = horizontalInput/second
			// Notice! We just changed what horizontalInput Stands for. It was previously 
			inputVector.x = horizontalInput * horizontalMoveSpeed * Time.deltaTime;

			//6. Basic Forward Movement
			inputVector.y = forwardSpeed * Time.deltaTime;


			// Apply Movement to the ship
			playerShipTransform.Translate(inputVector);

			// put bounds on the ship movement
			// You can't assign the individual x,y,z values directly, but if you can make a copy, and assign the values to it. 
			Vector3 shipPosition = playerShipTransform.position;

			if (shipPosition.x > maxHorizontalDistance) {
				shipPosition.x = maxHorizontalDistance;
			} else if (shipPosition.x < maxHorizontalDistance * -1f) {
				shipPosition.x = maxHorizontalDistance * -1f;
			}
			playerShipTransform.position = shipPosition;

		} else if (gameHasEnded) {

			//6. Basic Forward Movement
			float speed = forwardSpeed * Time.deltaTime;

			float lerpedSpeed = Mathf.Lerp(speed, 0, lerpValue);

			lerpValue += Time.deltaTime * lerpSpeed;

			// end the game
			playerShipTransform.Translate( 0, lerpedSpeed, 0 );

		}else{
			// 2.
			// if the button is pressed, make the image red
			//	buttonDown = Input.GetButtonDown("Jump"); // One Frame, The first frame that the button is pressed

			if (buttonDown) {
				buttonImage.color = Color.blue;
				CloseTitleScreen();
			} else {
				
				buttonImage.color = Color.red;
			}
		}


		// 1.
		// Quits 
		if (Input.GetKeyDown(KeyCode.Q)) {
			Application.Quit();
		}

		if (Input.GetKeyDown(KeyCode.R)) {
			// Reloads the Application
			SceneManager.LoadScene(0);
		}
	}
}
