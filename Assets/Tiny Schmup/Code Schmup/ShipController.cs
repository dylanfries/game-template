﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour {

	public StoryManager storyManager;

	public GameController gameController;


	void Start(){
		if (gameController == null) {
			Debug.LogWarning("Ship has no controller reference");
		}
	}

	// When we hit another collider
	private void OnCollisionEnter2D(Collision2D collision){

		if( collision.gameObject.CompareTag("Enemy")){
			// Hit an enemy
			Debug.Log("Hit n Enemy");

			gameController.HitAnEnemy( collision.gameObject );

		}else if( collision.gameObject.CompareTag("Rocks")){
			Debug.Log("Hit A Rock");

			gameController.HitARock();
		}



	}

	// Story beat
	private void OnTriggerEnter2D(Collider2D collision)
	{
		// you could also check for a tag 
//		collision.CompareTag("Object");

		// Check the collision for a story beat
		StoryBeat beat = collision.gameObject.GetComponent<StoryBeat>();

		if(beat.storyBead_EndGame){
			// let the game manager know something key happened
			gameController.GameEndedSuccessfully(beat);
		}else{
			// don't change the game but record the story to the UI
			storyManager.StoryEvent(beat);			
		}


	}

}
