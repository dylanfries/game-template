﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {


	public Transform followTarget;
	// Use this for initialization

	// Update is called once per frame
	void LateUpdate () {
		if( followTarget != null){
			transform.position = followTarget.position;
		}		
	}
}
