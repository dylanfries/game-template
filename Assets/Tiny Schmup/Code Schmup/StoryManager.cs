﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoryManager : MonoBehaviour {

	public Animator animator;

	// The whole panel 
	public GameObject storyPanel;

	// This is the UI element reference. we set printedStoryBeat.text = a string and it will display on the screen
	public Text printedStoryBeat;


	public string lineZero = "Hello World";

	// 
	//public void StoryHappens(string beatString){
	//}

	// A story thing happens somewhere and reports it to the story manager
	public void StoryEvent(StoryBeat beat){
		// Story Panel
		storyPanel.SetActive(true);
		// assign the value of line zero to the text 
		printedStoryBeat.text = beat.storyBeatText;

		// One way to do it
		//if( beat.storyBead_EndGame ){
		//	//	GameEnded
		//	SuccessfulEnd();
		//}
	}


	// Use this for initialization
	void Start () {
		
	}


	public void GameBegins (){
		// Story Panel
		storyPanel.SetActive(true);
		// assign the value of line zero to the text 
		printedStoryBeat.text = lineZero;


		// Trigger the Animator
		animator.SetTrigger("GameStarted");

	}

	public void GameCrashedEnding(){
		storyPanel.SetActive(true);
		printedStoryBeat.text = "Player Dies. Crashed into a rock";
		animator.SetTrigger("GameEnded");
	}

	public void EnemyKilledPlayerEnding(GameObject go){
		storyPanel.SetActive(true);
		printedStoryBeat.text = "Player Dies. Crushed by an enemy " + go.name;
		animator.SetTrigger("GameEnded");
	}

	public void SuccessfulEnd(StoryBeat beat){
		storyPanel.SetActive(true);
		printedStoryBeat.text = beat.storyBeatText;
		animator.SetTrigger("GameEnded");

	}

	// Update is called once per frame
	void Update () {
		
	}
}
