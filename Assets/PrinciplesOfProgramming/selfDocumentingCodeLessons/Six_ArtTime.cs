﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Six_ArtTime : MonoBehaviour {

	[Header("Art and Effects")]
	// The component that draws a 2d art panel to the screen, 
	// [!] Doesn't require a canvas to render
	public SpriteRenderer spriteRenderer; 

	// Animation Controller
	public Animator animator; 

	// The Particle System controls particles. My personal fav
	public ParticleSystem particleSystem; // particles

	[Header("Sample Art to set things to")]
	public Sprite sampleImage;

	// Use this for initialization
	void Start () {
		if( spriteRenderer == null ){
			Debug.LogWarning("Sprite Renderer Reference in Six_ArtTime is not set up. Don't forget to drag the reference in!");
		}
	}
	
	public void UpdateSprite(){
		// Assign an image to the spriteRenderer
		spriteRenderer.sprite = sampleImage;
		// Assign a color to the sprite renderer
		spriteRenderer.color = Color.red;

		// Red = 100%, Green = 20%, Blue = 0%, Alpha = 100%
		spriteRenderer.color = new Color(1f, .2f, 0, 1f); 
	}

	public void ParticleBasics(){
		//particleSystem
		// Basic Setup

		// Assign a default material ( I use this one 95% of the time)
		// Renderer > Material = DefaultParticle

		// Method 1:
		// Starts the particle system playing, plays a full cycle
		// Lifetime should be set, as well as play on start disabled. 
		// The particle system will need to have emission checkbox enabled
		particleSystem.Play();

		// Method 2: 
		// Emit 20 particles right this frame
		particleSystem.Emit(20); 
	

	}

	public void AnimatorBasics(){
		
	}

}
