﻿// Importing libraries. Don't worry about this just yet
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This is a comment. It is ignored

// This introduces Classes, Functions and Variables (all at once! Sorry!)

// This is a class declaration. Don't worry about MonoBehaviour yet
public class HelloWorld : MonoBehaviour {
	
	// basic variable test

	int numberTestVariable = 12345;

	// [ ] Try making these public, what happens? 

	// Public variables are accessible in the editor
	public bool printAdditionalMessage = false;
	public string additionalMessage = "Oh, are you still here?";

	// Use this for initialization
	// Start is a Unity function (I - its part of MonoBehaviour)
	void Start () {
		
		// 1. Debug.Log prints a string to the console. the "" mean this is a string
		Debug.Log("Hello World");

		string stringVariable = "Good Morning Human, Welcome to the training program";

		// Debug.Log(string messageName) is a function that prints a message to the console
		Debug.Log(stringVariable);

		// ToString() is a common function that returns a string
		Debug.Log(numberTestVariable.ToString());


		// ===== Intro To Functions =====

		// Test Calling a function
		AdditionalWelcome();

		// intro to control structures
		if(printAdditionalMessage){
			MoreAdditionalWelcomes();
		}

	}

	// A function that prints a welcome message and
	void AdditionalWelcome(){


	}

	void MoreAdditionalWelcomes(){
		Debug.Log(additionalMessage);
	}

}
