﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Required for UI
using UnityEngine.UI;

public class Five_AccessingComponents : MonoBehaviour {

	[Header("Basic Objects")]
	// GameObject is the base object of all (MonoBehaviour derived) objects. Anything in the scene is a GameObject
	public GameObject cachedGameobject;
	// Transform is the Position/Rotation/Scale of all scene gameObjects. 
	public Transform cachedTransform;

	[Header("Physics")]
	public Rigidbody2D rigid2D; // 2D Version
	public Rigidbody rigid3D; // 3D version

	// [!] 2D and 3D physics won't interact I just put them both here for demo purposes

	public Collider2D collider; // Can accept all types of 2D colliders
															// Can be more specific, eg 
															// public CircleCollider2D circleCollider;

	[Header("UI Elements")]
	// Using UI requires the library import
	public Text uiMessage; // UI Text
	public Image uiImage; // UI Image
	public Button uiButton; // etc

	[Header("Sample Art to set things to")]
	public Sprite sampleImage;

	void Awake(){
		// ------ Accessing Components ------
		cachedGameobject = GetComponent<GameObject>();
		cachedTransform = GetComponent<Transform>();
		// Also functions for getting components in children, parents and grabbing all of a type
	}



	public void BasicGameObject(){
		
		// ------ Basics ------
		// This enables and disables the gameObject. Useful all the time. Also works on UI elements
		gameObject.SetActive(true);

		// Set or get the name of the GameObject (The name in the scene and hierarchy)
		gameObject.name = "NewName";

		// ------ Checking for activation ------
		// Is the gameobjects own checkbox checked to be active? 
		bool isActiveSelf = gameObject.activeSelf;
		// Is the gameobject active in the hierarchy (eg if the GO parent is disabled, this will be false)
		bool isActiveInHierarchy = gameObject.activeInHierarchy;

		// ------ Instantiation ------
		// gameObject
		// ------ Destruction ------
		// Destroy(gameObject); // Destroys the game object
		// Destroy(gameObject, 2f); // Destroys the game object in 2s, useful for letting child effects play out before destroying
	}

	// ******************************************
	// The Transform is for holding and adjusting the position, rotation and scale of the gameobjects
	// It also holds the hierarchy reference for parenting
	// ******************************************
	public void BasicTransformAccessors(){
		// ------ Position ------
		// absolute world position
		Vector3 worldPosition = transform.position; 
		// Relative position offset from parent
		Vector3 offsetFromParent = transform.localPosition;


		// ------ Rotations ------
		// We will deal with this more in the Math section, for now
		// Quaternion - This is the datatype that stores the rotation internally. You don't really need to know what this is
		// Euler - This is the familiar Vector3 represention of rotation (that shows up in a transform), we will mostly use this for now
		Vector3 rotationAsEuler = transform.localEulerAngles;
		Quaternion qRotation = transform.rotation; // [!] Don't worry about this yet

		// ------ Scale ------
		// Scale relative to the world
		Vector3 scaleGlobal = transform.lossyScale;
		// Local scale. I mostly use this
		Vector3 localScale = transform.localScale;

		// ------ Parenting ------
		// Sets the parent
		// cachedTransform.SetParent(Transform parent);
	

	}

	public void AdjustingTransforms(){
		Vector3 movementInput = Vector3.up;
		// Moves per frame
		cachedTransform.Translate( movementInput );

		// moves per second
		cachedTransform.Translate( movementInput * Time.deltaTime );

		// Rotates euler degrees per frame
		cachedTransform.Rotate( movementInput );

		// rotates euler degrees per second
		cachedTransform.Rotate( movementInput * Time.deltaTime );
	}

	// This test function just shows some common ways of accessing the UI elements
	public void UseUI(){
		uiMessage.text = "String Here will set the text property";
		uiMessage.color = Color.red; // This will set the color to red

		// RGBA - values are counted as 0-1 rather then 0-255
		// an alpha of 1 is fully opaque, an alpha of 0 is fully transparent
		uiImage.color = new Color(1f, .75f, 0, 1f); // This Color is  100% Red, 75% Green, 0% Blue and 100% Opaque (Alpha)
		uiImage.sprite = sampleImage; // Assigns the sample image to the UI image

	}
	// ******************************************
	// Physics 2D
	// ******************************************
	public void AdjustPhysics2D(){
		// ----- Force -----
		// Add force to the 2d rigid. 
		// Add force should be called within a FixedUpdate loop
		Vector2 forceVector2D = new Vector2(1f, 3f);
		rigid2D.AddForce(forceVector2D * Time.deltaTime);


		float torque2D = 5f;
		// For making the rigidbody rotate using force
		rigid2D.AddTorque(torque2D);

		// Increase or decrease the drag. 
		// A value of 0 has no drag, and value of 0.5f is normally decent, up to 5+ it slows down quite quickly
		// Drag is useful to adjust to control input whne using force based input. 
		// We'll see this again in the Input & Control Module
		rigid2D.drag = 1f;

		// Disables physics simulation. This includes collisions, but also OnCollision and OnTrigger 
		// Only on the 2D physics. This is a cheaper way of enabling and disabling physics then enabling and disabling colliders
		rigid2D.simulated = false;

		Vector2 velocity = rigid2D.velocity; // includes directional
		float velocityMagnitude = velocity.magnitude; // just the speed, not directional
		Vector2 normalizedVelocity = rigid2D.velocity.normalized; // normalized to 1, this gives you the direction without the magnitude. 
		// We'll explore this stuff in way more detail in the Input course, but for now, recognize that its easier to control math if its been normalized. 
	}

	// Physics 3D
	public void AdjustPhysics3D(){
		// Add force to the 3D rigidbody
		Vector3 forceVector3D = new Vector3(1f, 2f, 3f);
		rigid3D.AddForce(forceVector3D);

		// mostly the same as 2D
	}

	// ******************************************
	// [!] Advanced stuff - Unity Events & Listening to Buttons
	// ******************************************
	// Register Button Callbacks
	// Normally I would just do this in the editor for a jam or prototype then code it in later
	// OnEnable - Like Start, its a MonoBehaviour function. This one gets called when 
	void OnEnable() {
		uiButton.onClick.AddListener(ClickedButtonFunction);
	}

	void OnDisable() {
		uiButton.onClick.RemoveListener(ClickedButtonFunction);
	}

	// Advanced: This Function will get called by the Button Click because it was assigned as a listener in the OnEnable function
	private void ClickedButtonFunction(){
		// Generate a random Color
		uiImage.color = Random.ColorHSV(0, 1f, 0, 1f, 0, 1f, 1f, 1f);

		// [!] I could have just used Random.ColorHSV(); but the alpha would have been randomized as well
	
	}
}
