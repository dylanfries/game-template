﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Taking Control of your control structures
// Concepts introduced: If / Else statements, while loops, for loops
public class Three_TakingControl : MonoBehaviour {

	public bool triggeringVariable = false;

	// Use this for initialization
	void Start () {
		
		// ------------------------
		// If 
		// If statements are a special kind of function. They are fundamental to programming, an atomic building block of logical structures
		// They take one parameter that is a logical operator (bool) and if it returns true, will execute the statement, if not it skips it
		// ------------------------
		if( true ){
			Debug.Log("Always Do This");
		}

		// ------------------------
		// Else
		// the If/Else statement is an extension of the basic If. It adds the else, which is a block of code that gets executed if the statement passed is false
		// ------------------------
		if( false ){
			// Will never be triggered
		}else{
			Debug.Log("Function Triggered with Else");
		}

		// ----- Operations -----
		// Can also do simple operations
		// Equality Operater is equal to
		// (1 == 1) is true

		// [!] Bug Warning: A common bug is using only one = when doing equality. The '=' has another meaning, which is to assign the value to a variable
		bool boolVar = true; // assigns the value of 4 to the intNum Variable
		// If you use this in the loop param, you can overwrite your variable
		// EG
		if( boolVar == false ){
			// Do this if boolVar equals false
		}

		if( boolVar = false ){
			// Accidentially assign boolVar the value of false
		}

		// Greater then / Less then
		// (1 < 2) is true, (4 > 6) is false
		// (a <= b) Equal or less then, ( a >= b) 


		// ------------------------
		// While Loop
		// A while loop is like an if, but it will continue executing over and over again until it returns false


		// Common patterns
		// Flag - Have a variable 
		// [!] Rememeber the variable has to be set outside of the 'Scope' of the function (Outside the curly braces)
		bool enterTheLoop = true;
		while( enterTheLoop ){ // enter loop the first time
			// run until complete
			enterTheLoop = false; // because we set the var to false, it will fail the second time
		}

		// -----------------------------
		// A simple counter 
		// -----------------------------
		int count = 0; // programmers count from 0
		while(count < 5){
			
			Debug.Log("While Loop Counting: " + count.ToString());

			count = count + 1; // count is equal to count plus 1
			// [!I] Shorthand for this is count++; 
		}
		// [!] Note the execution order. In the first loop, the message is printed before the value is incremented. 
		// If you reversed the order, you would get 1,2,3,4,5 instead of 0,1,2,3,4



		// [!] Be aware, not having an exit from the while loop is called an Infinite Loop and it will lcok up and crash Unity if you do it so be warned
		// Development can be a messy business! Don't forget you're like the technician poking a screwdriver into the engine so be careful when editing
		// ------------------------
		//	while( true ){
		//		// Executes forever and locks up the machine.
		//	}


		// -----------------------------
		// For Loops
		// Basically just a shorthand for the above type of loop
		// -----------------------------
		for (int i = 0; i < 5; i++){
			Debug.Log("For Loop Counting: " + i.ToString());
		}
		// 'i' can be any name, but historically 'i' is used for loops.
		// [!] Although you can change the value of i, you shouldn't! The language will allow it but its considered Bad Form and programmers will poo poo your attempts to do so

		Debug.Log("Done all loops! Are you feeling Loopy yet?!");

	}
}
