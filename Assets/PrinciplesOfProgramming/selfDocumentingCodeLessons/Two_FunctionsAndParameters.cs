﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Two_FunctionsAndParameters : MonoBehaviour {

	string stringOutsideFunction = "Hi Hi";

	// Use this for initialization
	void Start () {
		
		// String Parameters
		FunctionWithString("Hello World Again");

		// Variables defined in a function are only accessible within that function
		string stringVar = "And One More Time!";
		FunctionWithString( stringVar );

		FunctionWithString(stringOutsideFunction);

		// Int parameters
		FunctionWithParameters(9);
		FunctionWithParameters(45);

		// Multiple Parameters
		FunctionWithThreeParameters(56, true, "String Here");


		// Return Types
		int num = FunctionThatReturnsInt();
		Debug.Log("Function That Returns Int: " + num);
	}

	// accepts a basic string
	void FunctionWithString(string stringIn){
		Debug.Log(stringIn);
	}

	// accepts and int
	void FunctionWithParameters(int nameOfVariable){
		Debug.Log("Function With Params: " + nameOfVariable.ToString());
	}

	// Accepts multiple parameters
	void FunctionWithThreeParameters(int numberParam, bool boolParam, string stringParam ){
		// variables passed as parameters are only accessible within the function 
		if( boolParam){
			Debug.Log(stringParam);
			Debug.Log(numberParam.ToString());
		}else{
			Debug.Log("Bool Param Was false");
		}
	}

	// Means its returns a variable using the 'return' keyword
	int FunctionThatReturnsInt(){
		int integerVariable = 5 + 6;
		return integerVariable;
	}

	// numberParam, param, and stringParam are gone here

}
